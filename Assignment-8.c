#include <stdio.h>
struct student {
    char firstName[50];
    char subject[60];
    float marks;
}
s[5];
int main() {
    int i;
    printf("INPUT details of students:\n");
    for (i = 0; i < 5; ++i) {
    printf("\n");
    printf("Enter first name: ");
	scanf("%s", s[i].firstName);
	printf("Enter the subject: ");
    scanf("%s", s[i].subject);
    printf("Enter marks: ");
    scanf("%f", &s[i].marks);
    }
    printf("\n\n\n");

    printf("OUTPUT details of students:\n");
    for (i = 0; i < 5; ++i) {
    printf("\n");
    printf("First name: ");
    puts(s[i].firstName);
	printf("Subject: ");
    puts(s[i].subject);
    printf("Marks: %.1f", s[i].marks);
    printf("\n");
    }
    return 0;
}
